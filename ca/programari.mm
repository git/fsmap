<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#000000" CREATED="1317556548265" ID="ID_1987649862" MODIFIED="1319899891793" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      PROGRAMARI
    </p>
    <p style="text-align: center">
      <i><font size="3">SOFTWARE</font></i>
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="20"/>
<hook NAME="accessories/plugins/CreationModificationPlugin_new.properties"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties"/>
<node COLOR="#0033ff" CREATED="1317569566688" HGAP="-94" ID="ID_1459260305" MODIFIED="1319899833009" POSITION="right" STYLE="fork" TEXT="ES DIVIDEIX EN" VSHIFT="7">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1317569656955" HGAP="19" ID="ID_1245517962" MODIFIED="1319893834270" STYLE="bubble" TEXT="PROGRAMES" VSHIFT="2">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1317579808051" HGAP="21" ID="ID_1276504854" MODIFIED="1319893783950" STYLE="fork" TEXT="com" VSHIFT="-8">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1317569682175" HGAP="30" ID="ID_1451392720" MODIFIED="1317581451391" STYLE="bubble" TEXT="APLICACIONS">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1317569690248" HGAP="35" ID="ID_1741600068" MODIFIED="1317581446077" STYLE="bubble" TEXT="SCRIPTS">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1317569693992" HGAP="35" ID="ID_1053285696" MODIFIED="1319153699526" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      FITXERS DE
    </p>
    <p style="text-align: center">
      CONFIGURACI&#211;&#160;
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1317569704631" HGAP="35" ID="ID_1450936801" MODIFIED="1317774474093" STYLE="bubble" VSHIFT="-5">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      IMATGES
    </p>
    <p>
      <font size="2">de l'aplicaci&#243;</font>
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1317581487576" HGAP="37" ID="ID_1648688587" MODIFIED="1317583886557" STYLE="bubble" TEXT="..." VSHIFT="-3"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1317569668591" HGAP="23" ID="ID_1688089496" MODIFIED="1319893837886" STYLE="bubble" TEXT="DADES" VSHIFT="15">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1317579808051" HGAP="38" ID="ID_651676376" MODIFIED="1319897448341" STYLE="fork" TEXT="com" VSHIFT="-9">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1317569682175" HGAP="45" ID="ID_1614938377" MODIFIED="1319899889921" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: left">
      IMATGES
    </p>
    <p style="text-align: left">
      &#160;&#160;per editar,
    </p>
    <p style="text-align: left">
      &#160;&#160;visualitzar ...
    </p>
  </body>
</html>
</richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1317569690248" HGAP="46" ID="ID_141733971" MODIFIED="1319893225261" STYLE="bubble" TEXT="SONS">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1317569701304" HGAP="44" ID="ID_1145950530" MODIFIED="1319893228653" STYLE="bubble" TEXT="MANUALS" VSHIFT="1">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1319152490861" HGAP="45" ID="ID_1871183067" MODIFIED="1319893232181" STYLE="bubble" VSHIFT="3">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: left">
      FORMATS
    </p>
    <p style="text-align: left">
      PROTOCOLS
    </p>
    <p style="text-align: left">
      EST&#192;NDARDS
    </p>
  </body>
</html>
</richcontent>
</node>
<node COLOR="#111111" CREATED="1317569704631" HGAP="45" ID="ID_379515771" MODIFIED="1319893238245" STYLE="bubble" TEXT="..." VSHIFT="-5">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1317562424150" HGAP="27" ID="ID_1897817801" MODIFIED="1319897514077" POSITION="left" STYLE="fork" VSHIFT="-11">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      SEGONS
    </p>
    <p style="text-align: center">
      EL SEU &#218;S
    </p>
  </body>
</html></richcontent>
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1317557208387" HGAP="24" ID="ID_386212683" MODIFIED="1317573297993" STYLE="bubble" TEXT="DESENVOLUPAMENT" VSHIFT="1">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1317558016834" HGAP="28" ID="ID_722245054" MODIFIED="1317570232785" STYLE="bubble" TEXT="EDUCACI&#xd3;" VSHIFT="1">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1317558031524" HGAP="29" ID="ID_318234974" MODIFIED="1317570241509" STYLE="bubble" TEXT="GR&#xc0;FICS" VSHIFT="1">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1317558044060" HGAP="32" ID="ID_416497026" MODIFIED="1317570096194" STYLE="bubble" TEXT="INTERNET">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1317558046883" HGAP="36" ID="ID_98643366" MODIFIED="1317570248136" STYLE="bubble" TEXT="JOCS">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1317558049155" HGAP="36" ID="ID_1691434170" MODIFIED="1317579450981" STYLE="bubble" TEXT="MULTIM&#xc8;DIA" VSHIFT="2">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1317558100323" HGAP="33" ID="ID_101036034" MODIFIED="1317570084057" STYLE="bubble" TEXT="OFICINA">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1317558072019" HGAP="34" ID="ID_1205702994" MODIFIED="1317570264759" STYLE="bubble" TEXT="PREFER&#xc8;NCIES" VSHIFT="1">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1317558074363" HGAP="36" ID="ID_1629376498" MODIFIED="1317570034234" STYLE="bubble" TEXT="SISTEMA" VSHIFT="-1">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1317558106315" HGAP="35" ID="ID_1407690739" MODIFIED="1317570272530" STYLE="bubble" TEXT="UTILITATS" VSHIFT="-35">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1317571183975" HGAP="65" ID="ID_106515795" MODIFIED="1319899837737" POSITION="right" STYLE="fork" VSHIFT="20">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      SEGONS ELS
    </p>
    <p style="text-align: center">
      USUARIS
    </p>
  </body>
</html></richcontent>
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1317581231578" HGAP="50" ID="ID_1451528906" MODIFIED="1319893868470" STYLE="bubble" VSHIFT="9">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      HORITZONTALS
    </p>
    <p style="text-align: center">
      <font color="#000000" size="3">tothom les utilitza</font>
    </p>
  </body>
</html></richcontent>
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1317581276217" HGAP="50" ID="ID_1908999760" MODIFIED="1319899851737" STYLE="bubble" VSHIFT="6">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      VERTICALS
    </p>
    <p style="text-align: center">
      <font color="#000000" size="3">p&#250;blic molt especific</font>
    </p>
  </body>
</html></richcontent>
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1317771885347" HGAP="-11" ID="ID_1730301988" MODIFIED="1319893961838" POSITION="left" STYLE="fork" VSHIFT="7">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      SEGONS COM
    </p>
    <p style="text-align: center">
      FUNCIONEN
    </p>
  </body>
</html>
</richcontent>
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1317772244503" HGAP="29" ID="ID_548764214" MODIFIED="1319892998392" STYLE="bubble" TEXT="APLICACIONS" VSHIFT="-1">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1317773483990" HGAP="29" ID="ID_1418674016" MODIFIED="1319893403245" STYLE="bubble" VSHIFT="6">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      ENTORNS
    </p>
    <p style="text-align: center">
      D'ESCRIPTORI
    </p>
    <p style="text-align: center">
      SISTEMES
    </p>
    <p style="text-align: center">
      DE FINESTRES
    </p>
  </body>
</html></richcontent>
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1317772250135" HGAP="30" ID="ID_1716703676" MODIFIED="1319893410349" STYLE="bubble" VSHIFT="11">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      BIBLIOTEQUES
    </p>
    <p style="text-align: center">
      <i><font color="#333333" size="4">LIBRERIES</font></i>
    </p>
  </body>
</html></richcontent>
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1317772254775" HGAP="24" ID="ID_1793233300" MODIFIED="1317775748445" STYLE="bubble" VSHIFT="-4">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      SISTEMES OPERATIUS
    </p>
    <p style="text-align: center">
      <font size="3" color="#333333">Els &quot;directors d'orquesta&quot; amb els </font>
    </p>
    <p style="text-align: center">
      CONTROLADORS
    </p>
    <p style="text-align: center">
      <i><font color="#333333" size="3">DRIVERS</font></i>
    </p>
  </body>
</html></richcontent>
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1317775115635" HGAP="19" ID="ID_522041508" MODIFIED="1317775218319" STYLE="fork" TEXT="com" VSHIFT="-8">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1317772498317" ID="ID_1122948183" MODIFIED="1317775218323" STYLE="bubble" TEXT="GNU/Linux">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1317772533205" ID="ID_1327590703" MODIFIED="1317775218323" STYLE="bubble" TEXT="GNU/BSD">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1317772537725" ID="ID_1011950726" MODIFIED="1317775218324" STYLE="bubble" TEXT="Unix">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1317772545653" ID="ID_883841067" MODIFIED="1317775218324" STYLE="bubble" TEXT="Windows">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1317775488377" ID="ID_1438059471" MODIFIED="1317776203969" STYLE="bubble" TEXT="Mac OS X"/>
<node COLOR="#111111" CREATED="1317772795219" ID="ID_1873466868" MODIFIED="1317775218325" STYLE="bubble" TEXT="...">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1317772260471" HGAP="32" ID="ID_876629372" MODIFIED="1317775218309" STYLE="bubble" TEXT="MALWARE">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1317772445517" HGAP="102" ID="ID_92824359" MODIFIED="1319897436317" STYLE="fork" TEXT="com" VSHIFT="-9">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1317772414957" ID="ID_1008867969" MODIFIED="1317775218312" STYLE="bubble" TEXT="VIRUS">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1317772419085" ID="ID_874567184" MODIFIED="1317775218312" STYLE="bubble" TEXT="CUCS">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1319894235922" ID="ID_1306906142" MODIFIED="1319894650244" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      TROIANS
    </p>
  </body>
</html>
</richcontent>
</node>
<node COLOR="#111111" CREATED="1317772422869" ID="ID_682634563" MODIFIED="1317775218313" STYLE="bubble" TEXT="SPYWARE">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1317772441845" ID="ID_1716561756" MODIFIED="1317775218313" STYLE="bubble" TEXT="...">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1317562384615" HGAP="59" ID="ID_1030567000" MODIFIED="1319897588613" POSITION="right" STYLE="fork" VSHIFT="-100">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      SEGONS
    </p>
    <p style="text-align: center">
      LA LLIC&#200;NCIA
    </p>
  </body>
</html></richcontent>
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1317557286632" HGAP="34" ID="ID_1222249794" MODIFIED="1317775996269" STYLE="bubble" TEXT="PRIVATIU" VSHIFT="6">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1317557306014" HGAP="34" ID="ID_1612727919" LINK="fsmap-ca.svg" MODIFIED="1317775998165" STYLE="bubble" TEXT="LLIURE" VSHIFT="12">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
</node>
</map>
